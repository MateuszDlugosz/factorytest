package colors;

@RegistrableColorFactory(registerAs = RgbColorPrototype.class)
public class RgbColorFactory implements ConcreteColorFactory<RgbColor, RgbColorPrototype> {
    public static final float MIN_COLOR_VALUE = 0;
    public static final float MAX_COLOR_VALUE = 255;

    public RgbColor createColor(RgbColorPrototype prototype) {
        return new RgbColor(
                prepareColorValue(prototype.getR()),
                prepareColorValue(prototype.getG()),
                prepareColorValue(prototype.getB())
        );
    }

    private float prepareColorValue(float value) {
        return clampColorValue(value) / MAX_COLOR_VALUE;
    }

    private float clampColorValue(float value) {
        return Math.max(MIN_COLOR_VALUE, Math.min(MAX_COLOR_VALUE, value));
    }
}
