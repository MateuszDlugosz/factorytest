package colors;

@RegistrableColorFactory(registerAs = HexColorPrototype.class)
public class HexColorFactory implements ConcreteColorFactory<HexColor, HexColorPrototype> {
    public HexColor createColor(HexColorPrototype prototype) {
        return new HexColor(prototype.getHexValue());
    }
}
