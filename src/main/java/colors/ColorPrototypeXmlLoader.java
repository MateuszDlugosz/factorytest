package colors;

import org.w3c.dom.Element;

import java.util.HashMap;
import java.util.Map;

public class ColorPrototypeXmlLoader {
    public static final String TYPE_ATTRIBUTE = "type";

    private final Map<String, ConcreteColorPrototypeXmlLoader> concreteLoaders
            = new HashMap<String, ConcreteColorPrototypeXmlLoader>();

    public ColorPrototypeXmlLoader(ConcreteColorPrototypeXmlLoader[] registrableLoaders) {
        for (ConcreteColorPrototypeXmlLoader registrableLoader : registrableLoaders)
            registerConcreteLoader(registrableLoader);
    }

    public void registerConcreteLoader(ConcreteColorPrototypeXmlLoader registrableLoader) {
        RegistrableColorPrototypeXmlLoader annotation
                = registrableLoader.getClass().getAnnotation(RegistrableColorPrototypeXmlLoader.class);
        if (annotation == null)
            throw new LoaderRegistrationException(String.format(
                    "Can not register %s loader, annotation %s not found.",
                    registrableLoader.getClass(),
                    RegistrableColorPrototypeXmlLoader.class
            ));
        concreteLoaders.put(annotation.registerAs(), registrableLoader);
    }

    public ColorPrototype loadColorPrototype(Element element) {
        String type = element.getAttribute("type");
        if (concreteLoaders.containsKey(type)) {
            return concreteLoaders.get(type).loadColorPrototype(element);
        } else {
            throw new LoaderNotFoundException(String.format(
                    "Can not load color prototype, loader of %s type not found.", type
            ));
        }
    }
}
