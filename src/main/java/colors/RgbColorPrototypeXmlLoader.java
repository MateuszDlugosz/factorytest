package colors;

import org.w3c.dom.Element;

@RegistrableColorPrototypeXmlLoader(registerAs = "RgbColor")
public class RgbColorPrototypeXmlLoader implements ConcreteColorPrototypeXmlLoader<RgbColorPrototype> {
    public static final String R_ATTRIBUTE = "r";
    public static final String G_ATTRIBUTE = "g";
    public static final String B_ATTRIBUTE = "b";

    public RgbColorPrototype loadColorPrototype(Element element) {
        return new RgbColorPrototype(
                Float.valueOf(element.getAttribute(R_ATTRIBUTE)),
                Float.valueOf(element.getAttribute(G_ATTRIBUTE)),
                Float.valueOf(element.getAttribute(B_ATTRIBUTE))
        );
    }
}
