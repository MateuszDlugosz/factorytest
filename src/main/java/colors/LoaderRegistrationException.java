package colors;

public class LoaderRegistrationException extends RuntimeException {
    public LoaderRegistrationException(String message) {
        super(message);
    }
}
