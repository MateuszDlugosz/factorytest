package colors;

public class LoaderNotFoundException extends RuntimeException {
    public LoaderNotFoundException(String message) {
        super(message);
    }
}
