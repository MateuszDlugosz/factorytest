package colors;

public interface ConcreteColorFactory<T extends Color, U extends ColorPrototype> {
    T createColor(U prototype);
}
