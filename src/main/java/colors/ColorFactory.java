package colors;

import java.util.HashMap;
import java.util.Map;

public class ColorFactory {
    private final Map<Class<? extends ColorPrototype>, ConcreteColorFactory> concreteFactories
            = new HashMap<Class<? extends ColorPrototype>, ConcreteColorFactory>();

    public ColorFactory(ConcreteColorFactory[] registrableFactories) {
        for (ConcreteColorFactory registrableFactory : registrableFactories)
            registerConcreteFactory(registrableFactory);
    }

    public void registerConcreteFactory(ConcreteColorFactory registrableFactory) {
        RegistrableColorFactory annotation
                = registrableFactory.getClass().getAnnotation(RegistrableColorFactory.class);
        if (annotation == null)
            throw new FactoryRegistrationException(String.format(
                    "Can not register %s factory, annotation %s not found.",
                    registrableFactory.getClass(),
                    RegistrableColorFactory.class
            ));
        concreteFactories.put(annotation.registerAs(), registrableFactory);
    }

    public Color createColor(ColorPrototype prototype) {
        if (concreteFactories.containsKey(prototype.getClass())) {
            return concreteFactories.get(prototype.getClass()).createColor(prototype);
        } else {
            throw new FactoryNotFoundException(String.format(
                    "Can not create color, factory of %s prototype not found.", prototype.getClass()
            ));
        }
    }
}
