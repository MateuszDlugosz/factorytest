package colors;

import org.w3c.dom.Element;

public interface ConcreteColorPrototypeXmlLoader<T extends ColorPrototype> {
    T loadColorPrototype(Element element);
}
