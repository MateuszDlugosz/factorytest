package colors;

import org.w3c.dom.Element;

@RegistrableColorPrototypeXmlLoader(registerAs = "HexColor")
public class HexColorPrototypeXmlLoader implements ConcreteColorPrototypeXmlLoader<HexColorPrototype> {
    public static final String HEX_VALUE_ELEMENT = "hexValue";

    public HexColorPrototype loadColorPrototype(Element element) {
        return new HexColorPrototype(element.getAttribute(HEX_VALUE_ELEMENT));
    }
}
