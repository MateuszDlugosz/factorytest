package colors;

public class HexColorPrototype implements ColorPrototype {
    private final String hexValue;

    public HexColorPrototype(String hexValue) {
        this.hexValue = hexValue;
    }

    public String getHexValue() {
        return hexValue;
    }
}
