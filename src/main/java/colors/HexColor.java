package colors;

public class HexColor implements Color {
    public final String hexValue;

    public HexColor(String hexValue) {
        this.hexValue = hexValue;
    }

    public String getHexValue() {
        return hexValue;
    }
}
