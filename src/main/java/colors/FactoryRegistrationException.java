package colors;

public class FactoryRegistrationException extends RuntimeException {
    public FactoryRegistrationException(String message) {
        super(message);
    }
}
