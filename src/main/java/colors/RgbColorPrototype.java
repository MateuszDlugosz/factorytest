package colors;

public class RgbColorPrototype implements ColorPrototype {
    private final float r;
    private final float g;
    private final float b;

    public RgbColorPrototype(float r, float g, float b) {
        this.r = r;
        this.g = g;
        this.b = b;
    }

    public float getR() {
        return r;
    }

    public float getG() {
        return g;
    }

    public float getB() {
        return b;
    }
}
