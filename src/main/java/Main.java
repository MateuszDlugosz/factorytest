import colors.*;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;

public class Main {
    public static void main(String[] args) throws ParserConfigurationException, IOException, SAXException {
        ColorFactory colorFactory = new ColorFactory(
                new ConcreteColorFactory[] {
                        new HexColorFactory(),
                        new RgbColorFactory()
                }
        );

        ColorPrototypeXmlLoader colorPrototypeXmlLoader = new ColorPrototypeXmlLoader(
                new ConcreteColorPrototypeXmlLoader[] {
                        new HexColorPrototypeXmlLoader(),
                        new RgbColorPrototypeXmlLoader()
                }
        );


        String xml = "<color type=\"RgbColor\" r=\"100\" g=\"100\" b=\"210\"></color>";
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        Document document = builder.parse(is);

        ColorPrototype colorPrototype = colorPrototypeXmlLoader.loadColorPrototype(document.getDocumentElement());
        Color color = colorFactory.createColor(colorPrototype);

        System.out.println(color);
    }
}
